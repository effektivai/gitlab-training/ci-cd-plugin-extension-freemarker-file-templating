## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/effektivai/gitlab-training/ci-cd-plugin-extension-freemarker-file-templating)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## CI CD Plugin Extension 

This guided exploration implements CI CD library code in the form of a plugin extension.  It also models the general concept of building a standardized, version controlled plugin extension in GitLab.

## Not A Production Solution

The following disclaimer is emited to the logs by this plugin.  When using it in your own projects, you must copy the repository or it's files and then you can remove the code that emits this statement.

"The original location for this plugin extension is for demonstration and learning purposes only.  There is no breaking change SLA and it may change at any time without notice. If you need to depend on it for production or other critical systems, please make your own isolated copy to depend on.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-02-26

* **GitLab Version Released On**: 12.6

* **GitLab Edition Required**: 

  * For overall solution: [![FC](../config-data-monorepo/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:

  * [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo/-/blob/master/.build-template.yml)

## Demonstrates These Design Requirements and Desirements

As a complete whole, this guided exploration requires at least the Free / Core (![FC](images/FC.png)) edition of GitLab.

- **Development Pattern:** a file templating engine that substitutes GitLab CI variables into a file where special template insertion markup is used.
- **GitLab Development Pattern:** a plugin extension that consists of an includable GitLab CI yml job that is standardized and reusable.

#### GitLab CI Functionality:

- **.gitlab-ci.yml** "includes" ![FC](images/FC.png)can be a file in the same repo, another repo or an https url
  - Used to enable manage main build templates (gitlab ci custom functions) to have more change controls over them.
- **.gitlab-ci.yml** "extends" ![FC](images/FC.png)parameter for "template jobs" (gitlab ci custom functions).

## Using This Pattern:
- This repository contains a GitLab CI file showing the use of the plugin: [calling-plugin-sample.yml](./calling-plugin-sample.yml)
- The Guided Exploration [Config Data Monorepo](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo/-/blob/master/.build-template.yml) is a working pattern that implements this pattern.
- The comments in [ci-cd-plugin-extension-freemarker-file-templating.yml](ci-cd-plugin-extension-freemarker-file-templating.yml) provide additional guidance on usage.